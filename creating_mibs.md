## Free collection of mibs
* http://www.circitor.fr/Mibs/Html
* https://github.com/librenms/librenms/tree/master/mibs
 
* Cisco: ftp://ftp.cisco.com/pub/mibs/v2/v2.tar.gz
* APC: https://download.schneider-electric.com/files?p_File_Name=powernet426.mib
* Servertech: ftp://ftp.servertech.com/Pub/SNMP/sentry3/Sentry3.mib
* Palo Alto PanOS 7.0 enterprise MIBs: https://www.paloaltonetworks.com/content/dam/pan/en_US/assets/zip/technical-documentation/snmp-mib-modules/PAN-MIB-MODULES-7.0.zip
* Arista Networks: 
          * https://www.arista.com/assets/data/docs/MIBS/ARISTA-ENTITY-SENSOR-MIB.txt 
          * https://www.arista.com/assets/data/docs/MIBS/ARISTA-SW-IP-FORWARDING-MIB.txt 
          * https://www.arista.com/assets/data/docs/MIBS/ARISTA-SMI-MIB.txt
* Synology: https://global.download.synology.com/download/Document/Software/DeveloperGuide/Firmware/DSM/All/enu/Synology_MIB_File.zip
* MikroTik: http://download2.mikrotik.com/Mikrotik.mib
* UCD-SNMP-MIB (Net-SNMP): http://www.net-snmp.org/docs/mibs/UCD-SNMP-MIB.txt
* Ubiquiti Networks: http://dl.ubnt-ut.com/snmp/UBNT-MIB
          * http://dl.ubnt-ut.com/snmp/UBNT-UniFi-MIB
          * https://dl.ubnt.com/firmwares/airos-ubnt-mib/ubnt-mib.zip

## another collection of mibs (no download)
https://mibs.observium.org/

## Global OID reference database
https://oidref.com/

## Adding MIBS to config on server


Path to MIB modules /usr/share/snmp/mibs
Path for converted MIBS: /etc/snmp

> before converting copy origin mib-files to /usr/share/snmp/mibs to supress dependency errors


### converting MIB manually:
```
snmpttconvertmib --in=/PATH/MIBFILE.mib --out=/PATH/DefinitionFileName
```
 e.g.:
```
snmpttconvertmib --in=/tmp/SL_MIBS_2012-08-20/LSERIES-TAPE-LIBRARY-MIB.mib --out=/etc/snmp/snmptt_storagetek_LSERIES-TAPE-LIBRARY.txt
```

#### The Definitionfile has to be added to snmptt.conf (or sometimes snmptt.ini):

```
 vi /etc/snmp/snmptt.conf
 (vi /etc/snmp/snmptt.ini)
 
 [TrapFiles]
 # A list of snmptt.conf files (this is NOT the snmptrapd.conf file).  The COMPLETE path
 # and filename.  Ex: '/etc/snmp/snmptt.conf'
 snmptt_conf_files = <<END
 /PATH/DefinitionFileName
 e.g.: /etc/snmp/snmptt_storagetek_LSERIES-TAPE-LIBRARY.txt
 .
 .
 END
 ```

### or use script:


 ~~./convert_mibs -S PATHTOORIGIALMIBS -G PATHTOSTORECONVERTEDMIBS~~
 
 ~~e.g.~~
 ~~./convert_mibs -S /tmp/SL_MIBS_2012-08-20/compaq/ -G /etc/snmp/compaq/~~

after that, you have to restart the snmpt daemon:

```
/etc/init.d/snmptt restart
   Stopping /usr/sbin/snmptt                     done
   Starting /usr/sbin/snmptt                      done
 
```
 

### Testing traps by using OID or MIB-File:

SNMPv2 simplified the format of a notification request, consolidating everything within the varbind list, rather than having separate header fields just for Trap requests.  So the first two varbinds of an SNMPv2 notification will be sysUpTime.0 following by snmpTrapOID.0. The value of this second varbind is the OID identifying the trap being sent.

The snmptrap command will insert a sensible value for the sysUpTime varbind, so it's really just necessary to provide the trap OID (plus any additional varbinds from the OBJECTS clause):

```
snmptrap -v 2c -c public host "" UCD-NOTIFICATION-TEST-MIB::demoNotif SNMPv2-MIB::sysLocation.0 s "TEXT YOU WANT TO USE"
```

except of using the MIB-Text you can use the OID also

### example:

```
snmptrap -v 2c -c public XXX.XXX.XXX.XXX "" .1.3.6.1.4.1.2606.7.0.2 SNMPv2-MIB::sysLocation.0 s "TEXT YOU WANT TO USE"
```
 


## MIB Problems:

Be sure that you have all required (import) MIBS inside /usr/share/snmp/mibs

### Problem 1:

```
snmpttconvertmib --in=/tmp/SL_MIBS_2012-08-20/LSERIES-TAPE-LIBRARY-MIB.mib --out=/tmp/SL_MIBS_2012-08-20/snmptt_storagetek_LSERIES-TAPE-LIBRARY.txt
****  Processing MIB file ****
 snmptranslate version: NET-SNMP version: 5.4.2.1
 severity: Normal
 File to load is:        /tmp/SL_MIBS_2012-08-20/LSERIES-TAPE-LIBRARY-MIB.mib
 File to APPEND TO:      /tmp/SL_MIBS_2012-08-20/snmptt_storagetek_LSERIES-TAPE-LIBRARY.txt
 MIBS environment var:   /tmp/SL_MIBS_2012-08-20/LSERIES-TAPE-LIBRARY-MIB.mib
 mib name:

Aborting!!!
 Could not find DEFINITIONS ::= BEGIN statement in MIB file!
```


#### Solution:
 wrong phrase in original MIB-file:
```
LSERIES-TAPE-LIBRARY-MIB DEFINITIONS ::=
 BEGIN

corrected phrase:
 LSERIES-TAPE-LIBRARY-MIB DEFINITIONS ::= BEGIN
```


### Problem 2:

```
snmpttconvertmib --in=/usr/share/snmp/mibs/G3-AVAYA-TRAP.mib --out=/tmp/WFO_12_Framework_MIB_Files/converted/snmptt_G3-AVAYA-TRAPB.mib.txt

*****  Processing MIB file *****

snmptranslate version: NET-SNMP version: 5.4.2.1
severity: Normal

File to load is:        /usr/share/snmp/mibs/G3-AVAYA-TRAP.mib
File to APPEND TO:      /tmp/WFO_12_Framework_MIB_Files/converted/snmptt_G3-AVAYA-TRAPB.mib.txt

MIBS environment var:   /usr/share/snmp/mibs/G3-AVAYA-TRAP.mib
mib name: G3-AVAYA-MIB

Processing MIB:         G3-AVAYA-MIB

Processing MIB:         G3-AVAYA-TRAP
#
skipping a TRAP-TYPE / NOTIFICATION-TYPE line - probably an import line.
#
Line: 36784
TRAP-TYPE: alarmClear
Variables: g3clientExternalName g3alarmsProductID g3alarmsAlarmNumber
Looking up via snmptranslate: G3-AVAYA-TRAP::alarmClear
No log handling enabled - turning on stderr logging
Attempt to define a root oid (iso): At line 10 in /usr/share/snmp/mibs/G3-AVAYA-TRAP.mib
Bad parse of OBJECT IDENTIFIER: At line 10 in /usr/share/snmp/mibs/G3-AVAYA-TRAP.mib
Cannot find module (G3-AVAYA-TRAP): At line 1 in °?a
Unknown object identifier: G3-AVAYA-TRAP::alarmClear
OID:
```

#### Solution:

The problem is the translation of line 10 in the MIB-file. In this case:
```
iso             OBJECT IDENTIFIER ::= { 1 }
```

one solution is to comment it out (with "–")
```
-- iso             OBJECT IDENTIFIER ::= { 1 } 
```
 
## Create an own MIB file (example prometheus snmp webhook)

#### Result in Database
```
id          '27138963'
eventname   NULL
eventid     NULL
trapoid     'SNMPv2-SMI::enterprises.50495.15.1.2.1'
enterprise  ''
community   ''
hostname    'HOSTNAME'
agentip     'HOSTIPADDRESS'
category    NULL
severity    NULL
uptime      '0:0:00:00.00'
traptime    '2020-01-26 18:08:09'
formatline  '\\\'Unknown Trap: enterprises.50495.15.1.1.1 ():High cpu load enterprises.50495.15.1.1.2 ():firing enterprises.50495.15.1.1.3 ():warning enterprises.50495.15.1.1.7 ():{\\\"alert_name\\\": \\\"High cpu load\\\", \\\"alert_type\\\": \\\"metric\\\", \\\"cluster_name\\\": \\\"XXX-XXXXXX (ID: c-XXXXX)\\\", \\\"comparison\\\": \\\"greater than\\\", \\\"duration\\\": \\\"30s\\\", \\\"expression\\\": \\\"sum(node_load1) by (node)  / su'`
```

#### We start by collecting our unknown trap and take the information from snmptunknown.log logfile.

```
2020-01-26 18:08:09 : Unknown trap (.1.3.6.1.4.1.50495.15.1.2.1) received from HOSTNAME at:
Value 0: HOSTNAME
Value 1: HOSTIPADDRESS
Value 2: 0:0:00:00.00
Value 3: .1.3.6.1.4.1.50495.15.1.2.1
Value 4: HOSTIPADDRESS
Value 5:
Value 6:
Value 7:
Value 8:
Value 9:
Value 10:
Ent Value 0: .1.3.6.1.4.1.50495.15.1.1.1=High cpu load
Ent Value 1: .1.3.6.1.4.1.50495.15.1.1.2=firing
Ent Value 2: .1.3.6.1.4.1.50495.15.1.1.3=warning
Ent Value 3: .1.3.6.1.4.1.50495.15.1.1.7={"alert_name": "High cpu load", "alert_type": "metric", "cluster_name": "lph-prod01 (ID: c-XXXXX)", "comparison": "greater than", "duration": "30s", "expression": "sum(node_load1) by (node)  / sum(machine_cpu_cores)
 by (node) * 100>20", "group_id": "c-XXXXX:node-alert", "node": "HOSTNAME", "prometheus": "cattle-prometheus/cluster-monitoring", "prometheus_from": "lph-prod01", "rule_id": "c-XXXXX:node-alert_high-cpu-load", "threshold_value": "20"}
Ent Value 4: .1.3.6.1.4.1.50495.15.1.1.8=182:21:03:04.65
Ent Value 5: .1.3.6.1.4.1.50495.15.1.1.9={"status": "firing", "labels": {"alert_name": "High cpu load", "alert_type": "metric", "cluster_name": "lph-prod01 (ID: c-XXXXX)", "comparison": "greater than", "duration": "30s", "expression": "sum(node_load1) by (
node)  / sum(machine_cpu_cores) by (node) * 100>20", "group_id": "c-XXXXX:node-alert", "node": "HOSTNAME", "prometheus": "cattle-prometheus/cluster-monitoring", "prometheus_from": "lph-prod01", "rule_id": "c-XXXXX:node-alert_high-cpu-load", "threshold
_value": "20"}, "annotations": {"current_value": "26.25"}, "startsAt": "2020-01-26T17:07:45.880044763Z", "endsAt": "0001-01-01T00:00:00Z", "generatorURL": "http://prometheus-cluster-monitoring-0:9090/graph?g0.expr=sum+by%28node%29+%28node_load1%29+%2F+sum+
by%28node%29+%28machine_cpu_cores%29+%2A+100+%3E+20&g0.tab=1"}
```

#### Take just a configured MIB or copy the following one to a file 

```
EVENT n0braistAlarmTrap .1.3.6.1.4.1.50495.15.1.2.1 "Status Events" Normal
FORMAT SL Prometheus. $1 $3 ($2) (UNIXTime: $8). $7 --- RAW information: $9
SDESC
A trap for alarms sent by prometheus from SL container environment. The variable bindings
in the trap contain the detailed alarm information.
--1,3,6,1,4,1,50495,15,1,2,1
Variables:
  1: SL_Prometheus_Alarm_Name
  2: SL_Prometheus_Alarm_Status
  3: SL_Prometheus_Alarm_Severity
  4: SL_Prometheus_Alarm_UniqueIdentifier
  5: SL_Prometheus_Alarm_PrometheusJobName
  6: SL_Prometheus_Alarm_PrometheusAlertDescription
  7: SL_Prometheus_Alarm_PrometheusAlertLabelJSON
  8: SL_Prometheus_Alarm_UnixTimestamp
  9: SL_Prometheus_Alarm_RawAlertJSON
EDESC`

EVENT: Line where you have to add an nice name and the oid which is unknown. Also the TRAP category and TRAP severity information.
FORMAT: Line where you format the Trap text. Use free text and the variables given in the trap and order it like you want
SDESC: An information you can write in prosa
Variables: The informational part of the trap and a description what the variable means.
```

#### Save file an restart your services
```
service snmptt restart
service snmptrapd restart
```

 
#### Sending the trap (by hand)
```
snmptrap -v 2c -c public localhost '' 1.3.6.1.4.1.50495.15.1.2.1 1.3.6.1.4.1.50495.1.1.1 s SL_Prometheus_Alarm_Name 1.3.6.1.4.1.50495.1.1.2 s SL_Prometheus_Alarm_Status 1.3.6.1.4.1.50495.1.1.3 s SL_Prometheus_Alarm_Severity 1.3.6.1.4.1.50495.1.1.4 s SL_Prometheus_Alarm_UniqueIdentifier 1.3.6.1.4.1.50495.1.1.5 s SL_Prometheus_Alarm_PrometheusJobName 1.3.6.1.4.1.50495.1.1.6 s SL_Prometheus_Alarm_PrometheusAlertDescription 1.3.6.1.4.1.50495.1.1.7 s SL_Prometheus_Alarm_PrometheusAlertLabelJSON 1.3.6.1.4.1.50495.1.1.8 s SL_Prometheus_Alarm_UnixTimestamp 1.3.6.1.4.1.50495.1.1.9 s SL_Prometheus_Alarm_RawAlertJSON`
```

#### Result
```
id         '28025673'
eventname  'n0braistAlarmTrap'
eventid    '.1.3.6.1.4.1.50495.15.1.2.1'
trapoid    'SNMPv2-SMI::enterprises.50495.15.1.2.1'
enterprise ''
community  ''
hostname   'localhost'
agentip    '127.0.0.1'
category   'Status Events'
severity   'Normal'
uptime     '383:7:22:11.93'
traptime   '2020-05-25 16:58:03'
formatline 'SL Prometheus. SL_Prometheus_Alarm_Name SL_Prometheus_Alarm_Severity (SL_Prometheus_Alarm_Status) (UNIXTime:SL_Prometheus_Alarm_UnixTimestamp). SL_Prometheus_Alarm_PrometheusAlertLabelJSON.  RAW information: SL_Prometheus_Alarm_RawAlertJSON'`
```